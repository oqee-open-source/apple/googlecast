#!/bin/sh

# Exit on error
set -e

# Process the command line arguments

while getopts "i:o:" flag
do
    case "${flag}" in
        i) INPUT_PATH=${OPTARG};;
        o) OUTPUT_PATH=${OPTARG};;
    esac
done

if [ -z $INPUT_PATH ]; then
    INPUT_PATH="."
fi
echo "Input path $INPUT_PATH"

if [ -z $OUTPUT_PATH ]; then
    OUTPUT_PATH="$HOME/Desktop"
fi
echo "Output path $OUTPUT_PATH"

# Build the static libraries

xcodebuild archive -project ProtocolBuffers_iOS.xcodeproj -scheme ProtocolBuffers \
    -destination="iOS" -sdk iphoneos \
    -archivePath ${OUTPUT_PATH}/ProtocolBuffers-iphoneos \
    SKIP_INSTALL=NO BUILD_LIBRARIES_FOR_DISTRIBUTION=YES
    
xcodebuild archive -project ProtocolBuffers_iOS.xcodeproj -scheme ProtocolBuffers \
    -destination="iOS Simulator" -sdk iphonesimulator \
    -archivePath ${OUTPUT_PATH}/ProtocolBuffers-iphonesimulator \
    SKIP_INSTALL=NO BUILD_LIBRARIES_FOR_DISTRIBUTION=YES

# Gather the headers

mkdir ${OUTPUT_PATH}/Protobuf-headers
cp *.h ${OUTPUT_PATH}/Protobuf-headers/

# Generate the XCFramework

xcodebuild -create-xcframework \
    -library ${OUTPUT_PATH}/ProtocolBuffers-iphoneos.xcarchive/Products/usr/local/lib/libProtocolBuffers.a \
    -headers ${OUTPUT_PATH}/Protobuf-headers \
    -library ${OUTPUT_PATH}/ProtocolBuffers-iphonesimulator.xcarchive/Products/usr/local/lib/libProtocolBuffers.a \
    -headers ${OUTPUT_PATH}/Protobuf-headers \
    -output ${OUTPUT_PATH}/Protobuf.xcframework
