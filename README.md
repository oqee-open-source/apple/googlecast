# Google Cast

## Build Protobuf dependency

The Protobuf library is required by Google Cast. The XCFramework can be built manually from the source.

1. Download a version of the source code from the following page:
- https://github.com/protocolbuffers/protobuf/releases
The version currently used is v23.4 (https://github.com/protocolbuffers/protobuf/archive/refs/tags/v23.4.zip)

2. Execute the script `build-protobuf-xcframework.sh` from the folder `objectivec` in Protobuf source code.
This will build archives for iOS and iOS simulator and generate the XCFramework, the destination folder is the desktop by default.

## Retrieve Google Cast SDK

The latest version of the Google Cast SDK can be found on this page:
- https://developers.google.com/cast/docs/ios_sender

The static version should be selected.
